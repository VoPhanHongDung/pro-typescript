// sử dụng modules thay vì sử dụng namespace 
// chỉ cần export và import , đã có thể sử dụng modules 
export interface Ship {
    name: string;
    port: string;
    displacement: number;
   }
   export class Ferry implements Ship {
    constructor(
    public name: string,
    public port: string,
    public displacement: number) {
    }
   }

const defaultDisplacement = 4000;
class PrivateShip implements Ship {
 constructor(
    public name: string,
    public port: string,
    public displacement: number = defaultDisplacement) {
 }
}

// Import using an alias
// import { Ship as Boat } from './Listing-2-007';
// export class Dock {
//  private dockedShips: Boat[] = [];
//  arrival(ship: Boat) {
//  this.dockedShips.push(ship);
//  }
// }


export default class Yachted {
   
    constructor(
    public name: string,
    public port: string,
    public displacement: number) {
    }
   }

   //export default và named export 
   // ex default thằng cha đổi tên thì thằng import không cần đổi tên 
   // named ex thằng cha đổi tên thì thằng import đổi tên và phải đặt trong {}
   
