
//useful first
// namespace tạo vùng để gọi tên trùng vẫn ok 
namespace First {
    export class Example {
        log() {
            console.log('Logging from First.Example.log()');
        }
    }
}
namespace Second {
    export class Example {
        log() {
         console.log('Logging from Second.Example.log()');
        }
    }
}
const first = new First.Example();
   // Logging from First.Example.log()
first.log();
const second = new Second.Example();
   // Logging from Second.Example.log()
second.log();

//useful second
//dùng dấu chấm để phân cấp 
namespace FirstLevel {
    export namespace SecondLevel {
        export class Example {
            log() {
                console.log('Logging from SecondLevel.Example.log()');
               }
            }
        }
}
namespace FirstLevel.SecondLevel.ThirdLevel {
    export class Example {
        log() {
            console.log('Logging from ThirdLevel.Example.log()');
           }
    }
}
const nested = new FirstLevel.SecondLevel.Example();
const dotted = new FirstLevel.SecondLevel.ThirdLevel.Example();

//useful third 
namespace Shipping {
    // Available as Shipping.Ship
    export interface Ship {
    name: string;
    port: string;
    displacement: number;
}
    // Available as Shipping.Ferry
    export class Ferry implements Ship {
        constructor(
            public name: string,
            public port: string,
            public displacement: number) {}
}

 // Only available inside of the Shipping module
 const defaultDisplacement = 4000;
 class PrivateShip implements Ship {
 constructor(
 public name: string,
 public port: string,
 public displacement: number = defaultDisplacement) {
 }
 }
}

const ferry = new Shipping.Ferry('Assurance', 'London', 3220);

//useful fourth
//su dung import de keu thanh phan trong namespace khac 

namespace Docking {
    //import here 
    import Ship = Shipping.Ship;
    
    //------------------
    export class Dock {
    private dockedShips: Ship[] = [];
    arrival(ship: Ship) {
        this.dockedShips.push(ship);
        }
    }
}

const dock = new Docking.Dock()

//useful fiveth

// Class/Namespace Merging
class Car {}
namespace Car {
    export class Engine {
        }
    export class GloveBox {
        }
}
const car = new Car();
const engine = new Car.Engine();
const gloveBox = new Car.GloveBox();